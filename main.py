import json
import logging
import sqlite3
from typing import Iterator

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

ES_HOST = 'localhost'
ES_PORT = 9200
ES_INDEX_NAME = 'movies'
Q = """
    select
          json_object(
          '_id', m.id, 
          'id', m.id,
          'imdb_rating', m.imdb_rating,
          'genre', m.genre,
          'title', m.title,
          'description', m.plot,
          'director', m.director,
          'actors',(
            select
              json_group_array(json_object('id', a.id, 'name', a.name))
            from
              movie_actors ma
              JOIN actors a on ma.actor_id = a.id
            where
              movie_id = m.id
          ),
          'writers',(
            case when m.writer <> '' then 
            json_array(json_object('id', w.id, 'name', w.name))
            else (
              select
                json_group_array(json_object('id', w2.id, 'name', w2.name))
              from
                writers w2
                join (
                  select
                    json_extract(value, '$.id') as writer_id
                  from
                    json_each(m.writers, '$')
                ) on w2.id = writer_id
            ) end
            )
        )
        from
          movies m
      left join writers w on m.writer = w.id
    """
CHUNK_SIZE = 20

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Extract:

    def __init__(self):
        self.connection = sqlite3.connect("db.sqlite")
        self.cursor = self.connection.cursor()
        self.cursor.execute(Q)

    def __iter__(self):
        return self

    def __next__(self):
        chunk = self.cursor.fetchmany(CHUNK_SIZE)
        if chunk:
            return chunk
        self.connection.close()
        raise StopIteration


class Transform:

    def __init__(self, extract: Iterator):
        self.extract = extract

    def __iter__(self):
        return self

    def __next__(self):
        src_chunk = next(self.extract)
        return list(map(self.update_dict, src_chunk))

    @staticmethod
    def update_dict(src_row):
        src_dct = json.loads(src_row[0])
        src_dct['_index'] = ES_INDEX_NAME
        src_dct['actors'] = [i for i in src_dct['actors'] if i['name'] != 'N/A']
        src_dct['actors_names'] = ', '.join(i['name'] for i in src_dct['actors'])
        src_dct['writers'] = [i for i in src_dct['writers'] if i['name'] != 'N/A']
        src_dct['writers_names'] = ', '.join(i['name'] for i in src_dct['writers'])
        src_dct['genre'] = src_dct['genre'].split(', ')

        for key in src_dct.keys():
            src_dct[key] = None if src_dct[key] == 'N/A' else src_dct[key]

        return src_dct


class Load:

    def __init__(self, transform: Iterator):
        self.es = Elasticsearch([{'host': ES_HOST, 'port': ES_PORT}])
        self.transform = transform

    def __call__(self, *args, **kwargs):
        for i in self.transform:
            try:
                bulk(self.es, i)
                logger.info('%s loaded', len(i))
            except Exception as e:
                logger.exception('Error in %s', i)
                raise e


if __name__ == '__main__':
    Load(Transform(Extract()))()
